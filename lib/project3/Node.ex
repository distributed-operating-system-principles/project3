defmodule Project3.Node do
  use GenServer

  @call_duration 5000
  @time_duration 25
  @random_interval 15

  # ___________________________________________________API__________________________________________________________________________

  def start_link(from_pid, m, num_requests, opts \\ []) do
    GenServer.start_link(__MODULE__, {from_pid, m, num_requests}, opts)
  end

  def create_ring(node_pid) do
    GenServer.call(node_pid, :create_ring, @call_duration)
  end

  def join_ring(node_pid, chord_ring_pid) do
    GenServer.cast(node_pid, {:join_ring, chord_ring_pid})
  end

  # ________________________________________________Callbacks_______________________________________________________________________

  def init({from_pid, m, num_requests}) do
    finger_table = List.duplicate({nil, nil, 0}, m)

    node_idx =
      :crypto.hash(:sha, inspect(self()))
      |> binary_part(20 - div(m, 8), div(m, 8))
      |> :binary.decode_unsigned()

    # IO.puts("#{node_idx}")

    successor = nil
    predecessor = nil

    state = %{
      :successor => successor,
      :predecessor => predecessor,
      :finger_table => finger_table,
      :num_bits => m,
      :node_idx => node_idx,
      :next => 0,
      :storage => %{},
      :supervisor_pid => from_pid,
      :sent_to_supervisor => false,
      :num_requests => num_requests,
      :num_hops => 0,
      :requests_resolved => 0
    }

    {:ok, state}
  end

  def handle_call(:create_ring, _from, state) do
    state = Map.put(state, :predecessor, nil)
    state = Map.put(state, :successor, {Map.get(state, :node_idx), self(), 0})
    # IO.puts("#{Map.get(state, :node_idx)} created ring")
    Process.send_after(self(), :stabilize, time_duration())
    Process.send_after(self(), :check_predecessor, time_duration())
    Process.send_after(self(), :fix_fingers, time_duration())
    {:reply, :ok, state}
  end

  def handle_call(:predecessor, _from, state) do
    {:reply, Map.get(state, :predecessor), state}
  end

  def handle_cast({:set_successor, successor}, state) do
    # IO.puts("Changing successor")
    state = Map.put(state, :successor, successor)
    {:noreply, state}
  end

  def handle_cast({:kill_me, from_pid}, state) do
    predecessor = Map.get(state, :predecessor)
    successor = Map.get(state, :successor)
    node_idx = Map.get(state, :node_idx)

    if(predecessor !== nil) do
      GenServer.cast(elem(predecessor, 1), {:set_successor, successor})
    end

    # storage = Map.get(state, :storage)
    # rand_tuple = Enum.at(storage, 0)
    send(from_pid, {:killing_self, {node_idx, successor}})
    # Process.exit(self(), :kill)
    {:noreply, state}
  end

  def handle_cast({:put, key, value}, state) do
    # IO.puts("#{Map.get(state, :node_idx)} storing: #{key}")
    storage = Map.get(state, :storage)
    storage = Map.put(storage, key, value)
    state = Map.put(state, :storage, storage)
    {:noreply, state}
  end

  def handle_cast({:join_ring, chord_ring_pid}, state) do
    state = Map.put(state, :predecessor, nil)

    # IO.puts("#{Map.get(state, :node_idx)} trying to join")

    GenServer.cast(
      chord_ring_pid,
      {:find_successor, self(), Map.get(state, :node_idx), 1}
    )

    # IO.puts("#{Map.get(state, :node_idx)} joined. Successor = #{elem(successor, 0)}")

    state = Map.put(state, :chord_ring_pid, chord_ring_pid)
    {:noreply, state}
  end

  def handle_cast({:find_successor, pid, idx, i}, state) do
    successor = Map.get(state, :successor)
    node_idx = Map.get(state, :node_idx)
    m = Map.get(state, :num_bits)
    finger_table = Map.get(state, :finger_table)

    comp_idx =
      if(!is_tuple(i)) do
        pow2 = :math.pow(2, i - 1) |> trunc()
        powm = :math.pow(2, m) |> trunc()
        (idx + pow2) |> rem(powm)
      else
        idx
      end

    # IO.puts("#{node_idx}: For #{idx}")

    if(in_range(comp_idx, node_idx, elem(successor, 0)) || comp_idx === elem(successor, 0)) do
      GenServer.cast(pid, {:found_your_successor, i, successor})
    else
      {_ask_node_idx, ask_node_pid, _} =
        closest_preceding_node(comp_idx, node_idx, finger_table, m)

      # IO.puts("#{node_idx}: For #{idx}, #{i} is #{ask_node_idx}")
      GenServer.cast(ask_node_pid, {:find_successor, pid, idx, i})
    end

    {:noreply, state}
  end

  def handle_cast({:found_your_successor, i, successor}, state) do
    finger_table = Map.get(state, :finger_table)
    m = Map.get(state, :num_bits)
    finger_table = List.replace_at(finger_table, m - i, successor)

    state =
      if(
        Map.get(state, :sent_to_supervisor) === false &&
          finger_table === Map.get(state, :finger_table)
      ) do
        GenServer.cast(
          Map.get(state, :supervisor_pid),
          {:finger_table_done, {Map.get(state, :node_idx), finger_table}}
        )

        Map.put(state, :sent_to_supervisor, true)
      else
        state
      end

    state = Map.put(state, :finger_table, finger_table)

    # IO.puts("#{Map.get(state, :node_idx)} has finger table: #{Kernel.inspect(finger_table)}")

    state =
      if(i === 1 && Map.get(state, :successor) === nil && successor !== nil) do
        Process.send_after(self(), :stabilize, time_duration())
        Process.send_after(self(), :check_predecessor, time_duration())
        Process.send_after(self(), :fix_fingers, time_duration())

        # IO.puts("#{Map.get(state, :node_idx)} joined")

        Map.put(state, :successor, successor)
      else
        state
      end

    {:noreply, state}
  end

  def handle_cast({:get_hop_count, pid, key_hash, hop_count}, state) do
    successor = Map.get(state, :successor)
    node_idx = Map.get(state, :node_idx)
    m = Map.get(state, :num_bits)
    finger_table = Map.get(state, :finger_table)

    if(in_range(key_hash, node_idx, elem(successor, 0)) || key_hash === elem(successor, 0)) do
      GenServer.cast(pid, {:hop_count, key_hash, hop_count})
    else
      {_ask_node_idx, ask_node_pid, _} =
        closest_preceding_node(key_hash, node_idx, finger_table, m)

      # IO.puts("#{node_idx}: For #{idx}, #{i} is #{ask_node_idx}")
      GenServer.cast(ask_node_pid, {:get_hop_count, pid, key_hash, hop_count + 1})
    end

    {:noreply, state}
  end

  def handle_cast({:hop_count, _key_hash, hop_count}, state) do
    state = Map.put(state, :num_hops, Map.get(state, :num_hops) + hop_count)
    state = Map.put(state, :requests_resolved, Map.get(state, :requests_resolved) + 1)

    if(Map.get(state, :requests_resolved) === Map.get(state, :num_requests)) do
      GenServer.cast(
        Map.get(state, :supervisor_pid),
        {:avg_hops, Map.get(state, :num_hops) / Map.get(state, :num_requests)}
      )
    end

    {:noreply, state}
  end

  def handle_cast({:notify, {idx, pid}}, state) do
    predecessor = Map.get(state, :predecessor)
    node_idx = Map.get(state, :node_idx)

    predecessor =
      if predecessor === nil || in_range(idx, elem(predecessor, 0), node_idx) do
        {idx, pid, 0}
      else
        predecessor
      end

    # IO.puts("#{node_idx}'s predecessor: #{elem(predecessor, 0)}")

    state = Map.put(state, :predecessor, predecessor)
    {:noreply, state}
  end

  def handle_info(:stabilize, state) do
    successor = Map.get(state, :successor)
    successor_idx = elem(successor, 0)
    node_idx = Map.get(state, :node_idx)

    x =
      if(elem(successor, 1) === self()) do
        Map.get(state, :predecessor)
      else
        try do
          GenServer.call(elem(successor, 1), :predecessor, @call_duration)
        catch
          :exit, _ ->
            nil
        end
      end

    # IO.puts("x : #{Kernel.inspect(x)} for #{Map.get(state, :node_idx)}")

    successor =
      if(x !== nil && in_range(elem(x, 0), node_idx, successor_idx)) do
        x
      else
        successor
      end

    # IO.puts("#{Map.get(state, :node_idx)}'s successor: #{elem(successor, 0)}")

    # if(elem(successor, 1) !== self()) do
    # IO.puts("#{node_idx} notifying #{elem(successor, 0)}")
    GenServer.cast(elem(successor, 1), {:notify, {node_idx, self()}})
    # end

    state = Map.put(state, :successor, successor)

    Process.send_after(self(), :stabilize, time_duration())
    {:noreply, state}
  end

  def handle_info(:check_predecessor, state) do
    predecessor = Map.get(state, :predecessor)

    predecessor =
      if(predecessor !== nil && Process.alive?(elem(predecessor, 1)) === true) do
        predecessor
      else
        nil
      end

    state = Map.put(state, :predecessor, predecessor)

    Process.send_after(self(), :check_predecessor, time_duration())
    {:noreply, state}
  end

  def handle_info(:fix_fingers, state) do
    next = Map.get(state, :next)
    m = Map.get(state, :num_bits)
    node_idx = Map.get(state, :node_idx)
    next = next + 1

    next =
      if(next > m) do
        1
      else
        next
      end

    GenServer.cast(self(), {:find_successor, self(), node_idx, next})

    state = Map.put(state, :next, next)

    Process.send_after(self(), :fix_fingers, time_duration())
    {:noreply, state}
  end

  def handle_info(:start_requests, state) do
    num_requests = Map.get(state, :num_requests)
    m = Map.get(state, :num_bits)

    key_hash =
      :crypto.hash(:sha, Kernel.inspect(num_requests))
      |> binary_part(20 - div(m, 8), div(m, 8))
      |> :binary.decode_unsigned()

    GenServer.cast(self(), {:get_hop_count, self(), key_hash, 0})

    state = Map.put(state, :num_requests, Map.get(state, :num_requests) - 1)

    Process.send_after(self(), :start_requests, @time_duration)
    {:noreply, state}
  end

  def handle_info(_garbage, state) do
    # IO.puts("Got Garbage: #{Kernel.inspect(garbage)}")
    {:noreply, state}
  end

  # ___________________________________________________Private________________________________________________________________________

  defp time_duration() do
    @time_duration - @random_interval + :rand.uniform(2 * @random_interval)
  end

  defp closest_preceding_node(_idx, node_idx, [], _m) do
    {node_idx, self(), 0}
  end

  defp closest_preceding_node(idx, node_idx, finger_table, m) do
    [finger | finger_table] = finger_table

    retval =
      if(elem(finger, 0) !== nil && in_range(elem(finger, 0), node_idx, idx)) do
        # IO.puts("#{idx}: #{node_idx} has #{Kernel.inspect(finger)}")
        finger
      else
        closest_preceding_node(idx, node_idx, finger_table, m)
      end

    retval
  end

  defp in_range(val, first, last) do
    cond do
      first < last -> val > first && val < last
      true -> val > first || val < last
    end
  end
end
