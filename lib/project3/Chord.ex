defmodule Project3.Chord do
  use GenServer
  @startnodes_wait 3_600_000
  @timer_duration 0
  @timer_requests 0
  # ____________________________________________________________________API_______________________________________________________________

  def start_link(num_nodes, num_keys, obj, m, from_pid, opts \\ []) do
    GenServer.start_link(__MODULE__, {num_nodes, num_keys, obj, m, from_pid}, opts)
  end

  def start_nodes(chord_pid, num_requests) do
    GenServer.call(chord_pid, {:start_nodes, num_requests}, @startnodes_wait)
  end

  # def lookup(pid, key) do
  # end

  # _____________________________________________________________________CALLBACKS________________________________________________________
  def init({num_nodes, num_keys, obj, m, from_pid}) do
    state = %{
      :num_nodes => num_nodes,
      :num_keys => num_keys,
      :num_bits => m,
      :chord_nodes => [],
      :chord_ring_pid => nil,
      :received_finger_tables => 0,
      :val_obj => obj,
      :keys_inserted => 0,
      :avg_hops_recvd => 0,
      :total_hops => 0,
      :main_process => from_pid
    }

    {:ok, state}
  end

  def handle_cast(:insert_keys, state) do
    # IO.puts("Insert Keys")
    num_keys = Map.get(state, :num_keys)
    :ok = insert_keys(num_keys, state)
    {:noreply, state}
  end

  def handle_cast({:finger_table_done, {_node_idx, _finger_table}}, state) do
    # IO.puts("Finger Table Done")
    # IO.puts("#{node_idx} : #{Kernel.inspect(finger_table)}")
    state = Map.put(state, :received_finger_tables, Map.get(state, :received_finger_tables) + 1)

    if(Map.get(state, :received_finger_tables) === Map.get(state, :num_nodes)) do
      # IO.puts("CONVERGED!")
      GenServer.cast(self(), :insert_keys)
    end

    {:noreply, state}
  end

  def handle_cast({:found_your_successor, i, successor}, state) do
    # IO.puts("Found your successor")
    {key, value} = i
    GenServer.cast(elem(successor, 1), {:put, key, value})
    state = Map.put(state, :keys_inserted, Map.get(state, :keys_inserted) + 1)

    if(Map.get(state, :keys_inserted) === Map.get(state, :num_keys)) do
      GenServer.cast(self(), :start_requests)
    end

    {:noreply, state}
  end

  def handle_cast(:start_requests, state) do
    # IO.puts("Start requests")
    _chord_nodes = Map.get(state, :chord_nodes)
    # IO.puts("Starting requests: #{Kernel.inspect(chord_nodes)}")
    start_requests(Map.get(state, :chord_nodes))
    {:noreply, state}
  end

  def handle_cast(:decrease_num_nodes, state) do
    IO.puts("Decrease num_nodes")
    state = Map.put(state, :num_nodes, Map.get(state, :num_nodes) - 1)
    {:noreply, state}
  end

  def handle_cast({:avg_hops, hops}, state) do
    # IO.puts("Avg_hops")
    state = Map.put(state, :avg_hops_recvd, Map.get(state, :avg_hops_recvd) + 1)

    state = Map.put(state, :total_hops, Map.get(state, :total_hops) + hops)

    # IO.puts("#{hops}")

    if(Map.get(state, :avg_hops_recvd) === Map.get(state, :num_nodes)) do
      # IO.puts("#{Map.get(state, :total_hops) / Map.get(state, :num_nodes)}")

      send(
        Map.get(state, :main_process),
        {:final_result, Map.get(state, :total_hops) / Map.get(state, :num_nodes)}
      )
    end

    {:noreply, state}
  end

  def handle_call({:put, key, value}, _from, state) do
    # IO.puts("PUT CALL")
    chord_ring_pid = Map.get(state, :chord_ring_pid)
    m = Map.get(state, :num_bits)

    key_hash =
      :crypto.hash(:sha, inspect(self()))
      |> binary_part(20 - div(m, 8), div(m, 8))
      |> :binary.decode_unsigned()

    successor = GenServer.call(chord_ring_pid, {:find_successor, key_hash})
    :ok = GenServer.call(successor, {:put, key, value})
    {:reply, :ok, state}
  end

  def handle_call({:start_nodes, num_requests}, _from, state) do
    # IO.puts("START NODES CALL")
    num_nodes = Map.get(state, :num_nodes)
    m = Map.get(state, :num_bits)
    chord_nodes = Map.get(state, :chord_nodes)
    chord_ring_pid = Map.get(state, :chord_ring_pid)

    {chord_nodes, chord_ring_pid} =
      start_nodes(num_nodes, num_requests, {chord_nodes, m}, chord_ring_pid)

    state = Map.put(state, :chord_nodes, chord_nodes)
    state = Map.put(state, :chord_ring_pid, chord_ring_pid)

    {:reply, chord_nodes, state}
  end

  # _______________________________________________________________PRIVATE_______________________________________________________________
  defp start_nodes(0, _num_requests, {chord_nodes, _m}, chord_ring_pid) do
    {chord_nodes, chord_ring_pid}
  end

  defp start_nodes(num_nodes, num_requests, {chord_nodes, m}, chord_ring_pid) do
    {:ok, node_pid} = Project3.Node.start_link(self(), m, num_requests)
    # IO.puts(num_nodes)

    chord_ring_pid =
      if(chord_ring_pid === nil) do
        :ok = Project3.Node.create_ring(node_pid)
        node_pid
      else
        :ok = Project3.Node.join_ring(node_pid, chord_ring_pid)
        chord_ring_pid
      end

    chord_nodes = [node_pid | chord_nodes]
    :timer.sleep(@timer_duration)
    start_nodes(num_nodes - 1, num_requests, {chord_nodes, m}, chord_ring_pid)
  end

  defp insert_keys(0, _state) do
    :ok
  end

  defp insert_keys(num_keys, state) do
    chord_ring_pid = Map.get(state, :chord_ring_pid)
    m = Map.get(state, :num_bits)
    val_obj = Map.get(state, :val_obj)

    key_hash =
      :crypto.hash(:sha, Kernel.inspect(num_keys))
      |> binary_part(20 - div(m, 8), div(m, 8))
      |> :binary.decode_unsigned()

    GenServer.cast(
      chord_ring_pid,
      {:find_successor, self(), key_hash, {key_hash, val_obj <> Kernel.inspect(num_keys)}}
    )

    insert_keys(num_keys - 1, state)
  end

  defp start_requests([]) do
    :ok
  end

  defp start_requests(chord_nodes) do
    [chord_node | chord_nodes] = chord_nodes
    # GenServer.cast(chord_node, :start_requests)
    # IO.puts("Starting requests: #{Kernel.inspect(chord_node)}")

    if(Process.alive?(chord_node)) do
      Process.send_after(chord_node, :start_requests, @timer_requests)
    end

    start_requests(chord_nodes)
  end
end
