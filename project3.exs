args = System.argv()
num_nodes = elem(Integer.parse(Enum.at(args, 0)), 0)
num_requests = elem(Integer.parse(Enum.at(args, 1)), 0)

Process.flag(:trap_exit, true)

# num_nodes = 1500

# num_requests = 100

# IO.puts("#{num_nodes} #{num_requests}")

num_keys = num_nodes * 2

m = 32

obj = "Key Number: "

{:ok, chord_pid} = Project3.Chord.start_link(num_nodes, num_keys, obj, m, self())

chord_nodes = Project3.Chord.start_nodes(chord_pid, num_requests)

receive do
  {:final_result, x} -> IO.puts("#{x}")
end

kill_node = :rand.uniform(num_nodes)
kill_node_pid = Enum.at(chord_nodes, kill_node - 1)
IO.puts("Killing #{Kernel.inspect(kill_node_pid)}")

GenServer.cast(kill_node_pid, {:kill_me, self()})

{_key_to_search, _node_to_result} =
  receive do
    {:killing_self, {rand_key, successor}} -> {rand_key, successor}
  end

Process.exit(kill_node_pid, :kill)

IO.puts("Waiting for any problem in Stabilize")

:timer.sleep(5000)

# IO.puts("Successor of failed node: #{elem(node_to_result, 0)}")

# IO.puts("Key to search: #{Kernel.inspect(key_to_search)}")
# IO.puts("#{Kernel.inspect(chord_pid)}")
# GenServer.cast(chord_pid, :decrease_num_nodes)

# GenServer.cast(chord_pid, :start_requests)
# IO.puts("HELLO")

# receive do
# {:final_result, x} -> IO.puts("#{x}")
# end
